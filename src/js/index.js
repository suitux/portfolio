var typed = new Typed('.name-changing', {
    strings: ["", "Developer", "Architect", "Designer", "Xavier^2500"],
    typeSpeed: 50, 
    backSpeed: 35,
    loop: !0, 
    showCursor: true,
    cursorChar: "|" 
});

var map;
function initMap() {

    var myLatLng = {lat: 41.3617169, lng: 2.0944118};

    map = new google.maps.Map(document.getElementById('map'), {
        center: myLatLng,
        zoom: 16
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Xavi',
            draggable: false,
        animation: google.maps.Animation.DROP
    });

    marker.addListener('click', toggleBounce);
}

function toggleBounce() {
    if (marker.getAnimation() !== null) {
      marker.setAnimation(null);
    } else {
      marker.setAnimation(google.maps.Animation.BOUNCE);
    }
  }

$('document').ready(function(){
    $(".nav-link").on('click', function(e) {

        // prevent default anchor click behavior
        e.preventDefault();

        // store hash
        var hash = this.hash;

        // animate
        $('html, body').animate({
            scrollTop: $(hash).offset().top - 40
        }, 1000, function(){

            console.log($('.navbar').height());
            // when done, add hash to url
            // (default click behaviour)
            //window.location.hash = hash - $('.navbar').height();
        });

    });
});
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    function legacycoderules() { alert("You found it. Now, get back to work!!!"); }//LOL, you found the Easter Egg